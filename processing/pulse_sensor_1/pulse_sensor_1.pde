import processing.serial.*;
Serial myPort;        // The serial port

ArrayList< Float > values;
int BPM;
String needle = "/dev/ttyUSB0";

void setup() {

  size(1500, 450);

  values = new ArrayList< Float >();
  BPM = 0;

  int pID = -1;
  String [] ss = Serial.list();
  for ( int i = 0; i < ss.length; i++ ) {
    if ( ss[ i ].indexOf( needle ) == 0 ) {
      pID = i;
      break;
    }
  }

  if ( pID == -1 ) {
    System.exit( 0 );
  }

  println(Serial.list());
  myPort = new Serial(this, Serial.list()[ pID ], 115200);
  // don't generate a serialEvent() unless you get a newline character:
  myPort.bufferUntil('\n');
}

void draw() {

  if (myPort.available() > 0) {
    String inString = myPort.readStringUntil('\n');
    if (inString != null && inString.length() > 2 ) {
      inString = trim(inString);
      if ( inString.charAt(0) == 'S' ) {
        float inByte = float(inString.substring( 1 ));
        values.add( inByte );
      } else if ( inString.charAt(0) == 'B' ) {
        BPM = int(inString.substring( 1 ));  
      } else {
        println( inString );
      }
    }
  }
  while ( values.size () > 500 ) {
    values.remove( 0 );
  }

  background( 5 );

  strokeWeight( 2 );
  stroke( 255 );
  //translate( 0, mouseY );
  for ( int i = 0; i < values.size (); i++ ) {

    point( i * 3, map( values.get( i ), 200, 700, 0, height ) );
    if ( i > 0 )
      line( 
      ( i - 1 ) * 3, map( values.get( i - 1 ), 200, 700, 0, height ), 
      i * 3, map( values.get( i ), 200, 700, 0, height )  );
  }

  text( BPM, 10, 25 );
}

